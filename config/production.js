const config = {};

config.dbSettings = {
  host: process.env.DB_HOST || 'localhost',
  database: process.env.DATABASE || 'sphere-connection',
  user: process.env.USER || 'postgres',
  password: process.env.PASSWORD || 'Vologor111',
  port: process.env.PORT || 5432
};

config.cookie = {
  secure: process.env.SECURE || false,
};

config.hash = {
  salt: process.env.HASH_SALT || 'KDwvPSMgJFxKWgrnAtJYi370h52FZXSA',
  iterations: 50000,
  keylen: 128,
  digest: 'sha512'
};

config.authJwt = {
  secret: process.env.AUTHJWT_SECRET || 'cAd75ZdoFVpbpyuKFrtX1pk3Vn3PzP78'
};

config.apiPort = process.env.API_PORT || 3001;

module.exports = config;
