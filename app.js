'use strict';

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const favicon = require('static-favicon');
const helmet = require('helmet');
const logger = require('morgan');

const routes = require('./routes');

const app = express();

app.use(helmet());

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

app.use('/', routes);

module.exports = app;
