'use strict';

const Joi = require('joi');
const moment = require('moment');
const schemas = require('../bll/schemas/joi/index');

module.exports = {
  login: (req, res, next) => {
    Joi.validate({
      email: req.body.email,
      password: req.body.password
    }, schemas.auth.login, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                email: 'string',
                password: 'string'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  registration: (req, res, next) => {
    Joi.validate({
      email: req.body.email,
      password: req.body.password,
      confirmPassword: req.body.confirmPassword
    }, schemas.auth.registration, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                email: 'string',
                password: 'string',
                confirmPassword: 'string'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  }
}
