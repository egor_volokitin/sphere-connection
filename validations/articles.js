'use strict';

const Joi = require('joi');
const moment = require('moment');
const schemas = require('../bll/schemas/joi/index');

module.exports = {
  getContent: (req, res, next) => {
    Joi.validate({
      uuid: req.body.uuid
    }, schemas.articles.getContent, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                uuid: 'string'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  createArticle: (req, res, next) => {
    Joi.validate({
      content: req.body.content,
      title: req.body.title,
      published: req.body.published,
      shortDescription: req.body.shortDescription,
      previewImage: req.body.previewImage,
      commentsEnabled: req.body.commentsEnabled
    }, schemas.articles.createArticle, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                content: 'string',
                title: 'string',
                published: 'boolean',
                shortDescription: 'string [max: 120]',
                previewImage: 'string [max: 155, optional: true]',
                commentsEnabled: 'boolean'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  getTags: (req, res, next) => {
    Joi.validate({
      searchLine: req.body.searchLine
    }, schemas.articles.getTags, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                searchLine: 'string'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  articleReaded: (req, res, next) => {
    Joi.validate({
      uuid: req.body.uuid,
      fingerprint: req.body.fingerprint,
      url: req.body.url
    }, schemas.articles.articleReaded, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                uuid: 'string',
                fingerprint: 'string',
                url: 'string'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  articleViewed: (req, res, next) => {
    Joi.validate({
      uuid: req.body.uuid,
      fingerprint: req.body.fingerprint,
      url: req.body.url
    }, schemas.articles.articleViewed, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                uuid: 'string',
                fingerprint: 'string',
                url: 'string'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  getLastest: (req, res, next) => {
    Joi.validate({
      page: req.body.page
    }, schemas.articles.getLastest, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                page: 'int [min: 1]'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

  getBest: (req, res, next) => {
    Joi.validate({
      page: req.body.page,
      days: req.body.days
    }, schemas.articles.getBest, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                page: 'int [min: 1]',
                days: 'int [min: 1]'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },

}
