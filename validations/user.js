'use strict';

const Joi = require('joi');
const moment = require('moment');
const schemas = require('../bll/schemas/joi/index');

module.exports = {
  getUsersList: (req, res, next) => {
    Joi.validate({
      searchLine: req.body.searchLine,
      page: req.body.page
    }, schemas.user.getUsersList, err => {
      if(err) {
        console.error(err);
        res.status(200).json({
          response: {
            status: 'error',
            code: 'validation-error',
            message: 'Ошибка валидации запроса.',
            example: {
              body: {
                searchLine: 'string [min: 4]',
                page: 'int [min: 1]'
              }
            }
          },
          request: {
            body: req.body,
            date: moment().utc()
          }
        });
        return null;
      }

      next();
    });
  },
};
