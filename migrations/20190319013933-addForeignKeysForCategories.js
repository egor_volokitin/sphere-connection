'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.addForeignKey('categories', 'parent_categories', 'fk_categories-parent_categories', {
    'parent': 'id'
  }, {
    onDelete: 'CASCADE',
    onUpdate: 'RESTRICT'
  });
};

exports.down = function(db) {
  return db.removeForeignKey('categories', 'fk_categories-parent_categories');
};

exports._meta = {
  "version": 1
};
