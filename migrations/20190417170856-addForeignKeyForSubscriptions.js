'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.addForeignKey('subscriptions', 'users', 'fk_subscriptions-users', {
    'userId': 'id'
  }, {
    onDelete: 'CASCADE',
    onUpdate: 'RESTRICT'
  })
  .then(() => {
    return db.addForeignKey('subscriptions', 'users', 'fk_subscriptions-ref_users', {
      'refUserId': 'id'
    }, {
      onDelete: 'CASCADE',
      onUpdate: 'RESTRICT'
    });
  });
};

exports.down = function(db) {
  return db.removeForeignKey('subscriptions', 'fk_subscriptions-users')
  .then(() => db.removeForeignKey('subscriptions', 'fk_subscriptions-ref_users'))
};

exports._meta = {
  "version": 1
};
