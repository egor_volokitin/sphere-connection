'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('wallets', {
    id: { type: 'int', primaryKey: true, allowNull: false, autoIncrement: true },
    userId: { type: 'int', allowNull: false },
    currency: { type: 'string', allowNull: false },
    amount: { type: 'string', allowNull: false },
    createdAt: { type: 'timestamp' },
    updatedAt: { type: 'timestamp' }
  })
  .then(() => db.removeColumn('credentials', 'userBanned'));
};

exports.down = function(db) {
  return db.dropTable('wallets');
};

exports._meta = {
  "version": 1
};
