'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('credentials', {
    id: { type: 'int', primaryKey: true, allowNull: false, autoIncrement: true },
    userId: { type: 'int', allowNull: false },
    type: { type: 'string', allowNull: false },
    value: { type: 'string', allowNull: false,},
    userBanned: { type: 'boolean', defaultValue: false, allowNull: false },
    createdAt: { type: 'timestamp' },
    updatedAt: { type: 'timestamp' }
  });
};

exports.down = function(db) {
  return db.dropTable('credentials');
};

exports._meta = {
  "version": 1
};
