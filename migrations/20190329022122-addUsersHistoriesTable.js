'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('users_histories', {
    id: { type: 'int', primaryKey: true, allowNull: false, autoIncrement: true },
    action: { type: 'string' },
    fingerprint: { type: 'string' },
    url: { type: 'string' },
    createdAt: { type: 'timestamp' },
    updatedAt: { type: 'timestamp' }
  });
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
