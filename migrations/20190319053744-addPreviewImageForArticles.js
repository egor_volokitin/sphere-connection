'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('articles', {
    id: { type: 'int', primaryKey: true, allowNull: false, autoIncrement: true },
    userId: { type: 'int' },
    title: { type: 'text' },
    shortDesc: { type: 'text' },
    content: { type: 'text' },
    views: { type: 'int' },
    createdAt: { type: 'timestamp' },
    updatedAt: { type: 'timestamp' },
    published: { type: 'boolean' },
    uuid: { type: 'string' },
    previewImage: { type: 'string' }
  })
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
