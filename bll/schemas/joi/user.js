'use strict';

const Joi = require('joi');

module.exports = {
  getUsersList: Joi.object().keys({
    searchLine: Joi.string().max(154).required().allow(''),
    page: Joi.number().integer().min(1).required()
  })
}
