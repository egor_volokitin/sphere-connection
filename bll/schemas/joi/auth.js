'use strict';

const Joi = require('joi');

module.exports = {
  login: Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required()
  }),

  registration: Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().max(154).required(),
    confirmPassword: Joi.string().max(154).required()
  })
}
