'use strict';

const articles = require('./articles');
const auth = require('./auth');
const user = require('./user');

module.exports = {
  articles,
  auth,
  user
};
