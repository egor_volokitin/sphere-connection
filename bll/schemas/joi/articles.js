'use strict';

const Joi = require('joi');

module.exports = {
  getContent: Joi.object().keys({
    uuid: Joi.string().min(12).max(13).required()
  }),

  createArticle: Joi.object().keys({
    content: Joi.string().required(),
    title: Joi.string().required(),
    published: Joi.boolean().required(),
    shortDescription: Joi.string().required(),
    previewImage: Joi.string().empty().allow(null),
    commentsEnabled: Joi.boolean().required()
  }),

  getTags: Joi.object().keys({
    searchLine: Joi.string().required()
  }),

  articleReaded: Joi.object().keys({
    uuid: Joi.string().min(12).max(13).required(),
    fingerprint: Joi.string().required(),
    url: Joi.string().required()
  }),

  articleViewed: Joi.object().keys({
    uuid: Joi.string().min(12).max(13).required(),
    fingerprint: Joi.string().required(),
    url: Joi.string().required()
  }),

  getLastest: Joi.object().keys({
    page: Joi.number().integer().min(1).required()
  }),

  getBest: Joi.object().keys({
    page: Joi.number().integer().min(1).required(),
    days: Joi.number().integer().min(1).required()
  }),
}
