'use strict';

const consts = require('../constains');
const generate = require('nanoid/generate');
const sequelize = require('../../db');
const Sequelize = require('sequelize');

module.exports = {
  Users: sequelize.define('users', {
    email: { type: Sequelize.TEXT, allowNull: false },
    nickname: { type: Sequelize.TEXT, allowNull: false },
    role: { type: Sequelize.INTEGER, defaultValue: 0 },
    emailConfirmed: { type: Sequelize.BOOLEAN, defaultValue: false },
    userBanned: { type: Sequelize.BOOLEAN, defaultValue: false },
    password: {  type: Sequelize.TEXT },
    facebook: { type: Sequelize.STRING },
    vk: { type: Sequelize.STRING },
    twitter: { type: Sequelize.STRING },
    linkedin: { type: Sequelize.STRING },
    telegram: { type: Sequelize.STRING },
    image: { type: Sequelize.TEXT },
    fullRaiting: { type: Sequelize.INTEGER, defaultValue: 0 },
    freeRaiting: { type: Sequelize.INTEGER, defaultValue: 0 },
    status: { type: Sequelize.STRING },
    uuid: { type: Sequelize.STRING, defaultValue: generate(consts.nanoid.alphabet, 12) },
  }),

  Articles: sequelize.define('articles', {
    userId: { type: Sequelize.INTEGER },
    title: { type: Sequelize.TEXT },
    shortDesc: { type: Sequelize.TEXT },
    content: { type: Sequelize.TEXT },
    views: { type: Sequelize.INTEGER, defaultValue: 0 },
    published: { type: Sequelize.BOOLEAN },
    uuid: { type: Sequelize.STRING, allowNull: false },
    previewImage: { type: Sequelize.STRING },
    readed: { type: Sequelize.INTEGER, defaultValue: 0 },
    commentsEnabled: { type: Sequelize.BOOLEAN, defaultValue: true },
    likes: { type: Sequelize.INTEGER, defaultValue: 0 }
  }),

  Categories: sequelize.define('categories', {
    title: { type: Sequelize.STRING },
    image: { type: Sequelize.STRING },
    parent: { type: Sequelize.INTEGER, allowNull: false }
  }),

  ParentCategories: sequelize.define('parent_categories', {
    title: { type: Sequelize.STRING }
  }),

  UsersHistories: sequelize.define('users_histories', {
    action: { type: Sequelize.STRING },
    fingerprint: { type: Sequelize.STRING },
    url: { type: Sequelize.STRING },
    uuid: { type: Sequelize.STRING }
  }),

  Credentials: sequelize.define('credentials', {
    userId: { type: Sequelize.INTEGER },
    type: { type: Sequelize.STRING },
    value: { type: Sequelize.STRING },
  }),

  Wallets: sequelize.define('wallets', {
    userId: { type: Sequelize.INTEGER },
    currency: { type: Sequelize.STRING, defaultValue: 'usd' },
    amount: { type: Sequelize.INTEGER, defaultValue: 0 }
  }),

  Subscriptions: sequelize.define('subscriptions', {
    userId: { type: Sequelize.INTEGER },
    refUserId: { type: Sequelize.INTEGER }
  })
}
