'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');

/**
 * Создает jwt. Возвращает промис.
 * @param {Number} id Идентификатор пользователя в базе
 */
function createAuthJwt(id) {
  return new Promise(resolve => {
    resolve(jwt.sign({ id: id }, config.get('authJwt.secret')));
  });
}

module.exports = createAuthJwt;
