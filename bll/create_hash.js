'use strict';

const crypto = require('crypto');
const config = require('config');

const {
  salt,
  iterations,
  keylen,
  digest } = config.get('hash');

/**
 * Возвращает промис. Вернет хэш строки
 * @param {String} defaultString Хэшируемая строка
 */
function createHash(defaultString) {
  return new Promise(resolve => {
    crypto.pbkdf2(
      defaultString,
      salt,
      iterations,
      keylen,
      digest,
      (err, key) => {
        if(err) {
          console.error(err);
        }

        resolve(key.toString('hex'));
      }
    );
  });
}

module.exports = createHash;
