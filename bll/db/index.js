'use strict';

/**
 * Конфиг для настройки работы с базой.
 */

const config = require('config');
const Sequelize = require('sequelize');

const postgresSettings = config.get('dbSettings');

const { database, user, password, host } = postgresSettings;
const sequelize = new Sequelize(database, user, password, {
  host,
  dialect: 'postgres',
  operatorsAliases: false,

  /**
   * max: 1000 - Максимальное число коннектов к базе данных. При необходимости - увеличить.
   */  
  pool: {
    acquire: 20000,
    idle: 10000,
    max: 1000
  }
});

module.exports = sequelize;
