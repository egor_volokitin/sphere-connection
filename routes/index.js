'use strict';

const express = require('express');
const router = express.Router();

const validation = require('../validations');

const articles = require('../api/articles');
const auth = require('../api/auth');
const s3 = require('../api/s3');
const user = require('../api/user');

const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const amazon_s3 = new aws.S3({
  region: 'eu-central-1',
  accessKeyId: 'AKIAJAQ3XBDCB2UQD7VQ',
  secretAccessKey: '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'
});

const upload = multer({
  storage: multerS3({
    s3: amazon_s3,
    bucket: 'test-sconnection/avatars',
    acl: 'public-read',
  })
});

router.post('/app/articles/getContent', validation.articles.getContent, auth.isAuth, articles.getContent, (req, res) => {
  res.end();
});

router.post('/app/articles/readed', validation.articles.articleReaded, articles.articleReaded, (req, res) => {
  res.end();
});

router.post('/app/articles/viewed', validation.articles.articleViewed, articles.articleViewed, (req, res) => {
  res.end();
});

router.post('/app/articles/like', auth.isAuth, auth.closeUnauth, articles.like, (req, res) => {
  // res.end();
});

router.post('/app/articles/unlike', auth.isAuth, auth.closeUnauth, articles.unLike, (req, res) => {
  // res.end();
});

router.post('/app/articles/getLastest', validation.articles.getLastest, articles.getLastest, (req, res) => {
  res.end();
});

router.post('/app/articles/getBest', validation.articles.getBest, articles.getBest, (req, res) => {
  res.end();
});

router.post('/app/auth/isAuth', auth.isAuth, (req, res) => {
  res.status(200).json({
    response: {
      status: req.user.isAuth ? 'success' : 'error',
      code: req.user.isAuth ? null : 'unauthorized-user',
      message: {
        auth: req.user.isAuth
      }
    }
  });
});

router.post('/app/auth/login', validation.auth.login, auth.login, (req, res) => {
  res.end();
});

// no-validations //
router.post('/app/auth/logout', auth.logout, (req, res) => {
  res.end();
});

router.post('/app/auth/regitration', validation.auth.registration, auth.registration, (req, res) => {
  res.end();
});

router.post('/app/user/getUsersList', validation.user.getUsersList, user.getUsersList, (req, res) => {
  res.end();
});

// Эта функция вызывается каждый раз перед созданием статьи.
// Помимо хэша S3 она так же возвращает ник и аватар пользователя
// no-validations //
router.get('/app/s3/signature/get', auth.isAuth, auth.closeUnauth, s3.getSignature, (req, res) => {
  res.end();
});

router.post('/app/user/articles/create', validation.articles.createArticle, auth.isAuth, auth.closeUnauth, user.articles.createArticle, (req, res) => {
  res.end();
});

// no-validations //
router.post('/app/user/articles/getAll', auth.isAuth, auth.closeUnauth, user.articles.getAll, (req, res) => {
  res.end();
});

router.post('/app/user/articles/create/getTags', validation.articles.getTags, auth.isAuth, auth.closeUnauth, user.articles.getTags, (req, res) => {
  res.end();
});

router.post('/app/user/settings/getInfo', auth.isAuth, auth.closeUnauth, user.settings.getInfo, (req, res) => {
  // res.end();
});

router.post('/app/user/settings/saveMainInfo', auth.isAuth, auth.closeUnauth, upload.single('file'), user.settings.updateMainInfo, (req, res) => {
  // res.end();
});

router.post('/app/user/getInfoForMyPage', auth.isAuth, auth.closeUnauth, user.getInfoForMyPage, (req, res) => {
  // res.end();
});

router.post('/app/user/subscribe', auth.isAuth, auth.closeUnauth, user.subscriptions.subscribe, (req, res) => {
  // res.end();
});

router.post('/app/user/unsubscribe', auth.isAuth, auth.closeUnauth, user.subscriptions.unSubscribe, (req, res) => {
  // res.end();
});

module.exports = router;
