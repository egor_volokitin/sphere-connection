'use strict';

const FroalaEditor = require('wysiwyg-editor-node-sdk/lib/froalaEditor');
const schemas = require('../../bll/schemas/sequelize');

/**
 * Эта функция вызывается каждый раз при создании новой статьи
 * Поэтому можно вполне логично сюда добавить логику и возвращать аватар и ник пользователя
 */
module.exports = async (req, res, next) => {
  const config = {
    bucket: 'test-sconnection',
    region: 'eu-central-1',
    keyStart: 'uploads/',
    acl: 'public-read',
    accessKey: 'AKIAJAQ3XBDCB2UQD7VQ',
    secretKey: '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'
  };

  const hash = FroalaEditor.S3.getHash(config);

  const user = await schemas.Users.findOne({
    attributes: ['nickname', 'image'],
    where: {
      id: req.user.id
    },
    raw: true
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: {
        hash: hash,
        user: user
      }
    }
  });
}
