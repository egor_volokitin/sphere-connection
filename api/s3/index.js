'use strict';

const getSignature = require('./getSignature');

module.exports = {
  getSignature
};
