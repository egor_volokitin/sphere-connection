'use strict';

const schemas = require('../../bll/schemas/sequelize');
const moment = require('moment');
const Op = require('sequelize').Op;

module.exports = async(req, res, next) => {
  console.log(req.body.days);

  const startDay = moment().subtract(req.body.days, 'days').startOf('day').toDate();

  const articles = await schemas.Articles.findAll({
    attributes: ['title', ['shortDesc', 'shortDescription'], 'uuid', 'previewImage'],
    where: {
      createdAt: {
        [Op.between]: [startDay, moment().toDate()]
      },
      published: true
    },
    limit: 50,
    offset: (req.body.page - 1) * 50,
    order: [['likes', 'DESC']],
    raw: true
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: articles
    }
  })
}
