'use strict';

const articleReaded = require('./articleReaded');
const articleViewed = require('./articleViewed');
const getContent = require('./getContent');
const getLastest = require('./getLastest');
const getBest = require('./getBest');

const like = require('./like');
const unLike = require('./unLike');

module.exports = {

  articleReaded,
  articleViewed,
  getContent,
  getLastest,
  getBest,

  like,
  unLike
};
