'use strict';

const schemas = require('../../bll/schemas/sequelize');
const sequelize = require('sequelize');

module.exports = async (req, res, next) => {
  let user = await schemas.UsersHistories.findOne({
    attributes: ['id'],
    where: {
      action: 'view_article',
      uuid: req.body.uuid,
      fingerprint: req.body.fingerprint
    },
    raw: true
  });

  if(user) {
    res.status(200).json({
      response: {
        status: 'success',
        message: 'Ok'
      }
    });

    return null;
  }

  await schemas.UsersHistories.create({
    url: req.body.url,
    uuid: req.body.uuid,
    fingerprint: req.body.fingerprint,
    action: 'view_article'
  });

  await schemas.Articles.update({
    views: sequelize.literal('views + 1')
  }, {
    where: {
      uuid: req.body.uuid
    }
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: 'Ok'
    }
  });
}
