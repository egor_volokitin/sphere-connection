'use strict';

const schemas = require('../../bll/schemas/sequelize');
const sequelize = require('sequelize');

module.exports = async (req, res, next) => {

  // Получение контента и названия статьи
  let article = await schemas.Articles.findOne({
    attributes: ['userId', 'id'],
    where: {
      uuid: req.body.uuid
    },
    raw: true
  });

  if(!article) {
    res.status(404).json({
      response: {
        status: 'error',
        message: 'Статья не найдена'
      }
    });

    return null;
  }

  // Получаем владельца статьи
  const owner = await schemas.Users.findOne({
    attributes: ['nickname', 'image', 'uuid', 'id'],
    where: {
      id: article.userId
    },
    raw: true
  });

  let subscribeInfo = {
    subscribed: false,
  };

  if(req.user) {
    let sInfo = await schemas.Subscriptions.findOne({
      attributes: ['id'],
      where: {
        userId: req.user.id,
        refUserId: owner.id
      }
    });

    if(sInfo) {
      subscribeInfo.subscribed = true;
    }
  }

  await schemas.Articles.update({
    views: sequelize.literal('views + 1')
  }, {
    where: {
      id: article.id
    }
  });

  article = await schemas.Articles.findOne({
    attributes: ['title', 'uuid', 'content'],
    where: {
      id: article.id
    }
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: {
        article: article,
        article_owner: owner,
        sub_info: subscribeInfo
      }
    }
  });
}
