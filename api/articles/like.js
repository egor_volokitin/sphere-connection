'use strict';

const schemas = require('../../bll/schemas/sequelize');
const sequelize = require('sequelize');
const moment = require('moment');

module.exports = async (req, res, next) => {
  const article = await schemas.Articles.findOne({
    attributes: ['id', 'likes'],
    where: {
      uuid: req.body.uuid
    },
    raw: true
  });

  if(!article) {
    res.status(200).json({
      response: {
        status: 'error',
        code: 'invalid-data',
        message: 'Не удалось найти статью. Возможно она была удалена. Обновите страницу или попробуйте позднее.'
      },
      requset: {
        body: req.body,
        date: moment().utc().format('lll')
      }
    });

    return null;
  }

  await schemas.Articles.update({
    likes: sequelize.literal('likes + 1')
  }, {
    where: {
      id: article.id
    }
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: article.likes + 1
    }
  });
};
