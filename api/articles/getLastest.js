'use strict';

const schemas = require('../../bll/schemas/sequelize');

module.exports = async(req, res, next) => {
  const articles = await schemas.Articles.findAll({
    attributes: ['title', ['shortDesc', 'shortDescription'], 'uuid', 'previewImage'],
    where: {
      published: true
    },
    limit: 10,
    offset: (req.body.page - 1) * 10,
    order: [['createdAt', 'DESC']],
    raw: true
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: articles
    }
  });
}
