'use strict';

const config = require('config');
const createHash = require('../../bll/create_hash');
const createAuthJwt = require('../../bll/create_auth_jwt');
const moment = require('moment')
const schemas = require('../../bll/schemas/sequelize');

module.exports = async (req, res, next) => {
  const hash = await createHash(req.body.password);

  const user = await schemas.Users.findOne({
    attributes: ['id', 'password'],
    where: {
      email: req.body.email
    },
    raw: true
  });

  if(!user) {
    res.status(200).json({
      response: {
        status: 'error',
        code: 'login-error',
        message: `Пользователь с email ${req.body.email} не зарегистрирован.`
      },
      request: {
        body: req.body,
        date: moment.utc().format('lll')
      }
    });

    return null;
  }

  if(user.password !== hash) {
    res.status(200).json({
      response: {
        status: 'error',
        code: 'login-error',
        message: 'Не верный пароль.'
      },
      request: {
        body: req.body,
        date: moment.utc().format('lll')
      }
    });

    return null;
  }

  const token = await createAuthJwt(user.id);

  res.status(200).cookie('s_auth', token, {
    httpOnly: true,
    secure: config.get('cookie.secure')
  }).json({
    response: {
      status: 'success',
      message: 'Вход успешен'
    }
  });
};
