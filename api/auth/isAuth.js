'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = async (req, res, next) => {
  if(!req.cookies) {
    req.user = {
      isAuth: false
    }

  }

  if(!req.cookies.s_auth) {
    req.user = {
      isAuth: false,
    };

  }

  jwt.verify(req.cookies.s_auth, config.get('authJwt.secret'), (err, decoded) => {
    if(err) {
      res.clearCookie('s_auth');
      
      req.user = {
        isAuth: false
      };

      next();
    }
    else {
      req.user = {
        isAuth: true,
        id: decoded.id
      };

      next();
    }
  });
}
