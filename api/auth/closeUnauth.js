'use strict';

const moment = require('moment');

module.exports = async (req, res, next) => {
  if(!req.user.isAuth) {
    res.status(200).json({
      response: {
        status: 'error',
        code: 'unauthorized-user',
        message: 'Не удается выполнить запрос. Авторизуйтесь заново и попробуйте еще раз.'
      },
      request: {
        body: req.body,
        query: req.query,
        params: req.params,

        date: moment()
      }
    });

    return null;
  }

  next();
}
