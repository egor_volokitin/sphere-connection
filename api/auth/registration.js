'use strict';

const createHash = require('../../bll/create_hash');
const createJwt = require('../../bll/create_auth_jwt');
const moment = require('moment');
const schemas = require('../../bll/schemas/sequelize');

module.exports = async (req, res, next) => {

  if(req.body.password !== req.body.confirmPassword) {
    res.status(200).json({
      response: {
        status: 'error',
        code: 'registration-error',
        message: 'Пароли не совпадают.'
      },
      request: {
        body: req.body,
        date: moment.utc().format('lll')
      }
    });

    return null;
  }

  let user = await schemas.Users.findOne({
    attributes: ['id', 'password'],
    where: {
      email: req.body.email
    },
    raw: true
  });

  if(user) {
    const hash = await createHash(req.body.password);

    if(user.password === hash) {
      const token = await createJwt(user.id);

      res.status(200).cookie('s_auth', token, {
        httpOnly: true
      }).json({
        response: {
          status: 'success',
          message: 'Вход выполнен успешно.'
        }
      });

      return null;
    }

    res.status(200).json({
      response: {
        status: 'error',
        code: 'registration-error',
        message: `Пользователь с email ${req.body.email} уже зарегистрирован.`
      },
      request: {
        body: req.body,
        date: moment.utc().format('lll')
      }
    });

    return null;
  }

  const hash = await createHash(req.body.password);
  user = await schemas.Users.create({
    email: req.body.email,
    password: hash,
    nickname: req.body.email.split('@')[0] // ставит в ник все что шло до указания почты
  }, {
    raw: true
  });

  const token = await createJwt(user.id);

  await schemas.Wallets.findOrCreate({
    where: {
      userId: user.id,
      currency: 'usd'
    },
    defaults: {
      amount: 0
    }
  });

  res.status(200).cookie('s_auth', token, {
    httpOnly: true
  }).json({
    response: {
      status: 'success',
      message: 'Регистрация прошла успешно.'
    }
  });
}
