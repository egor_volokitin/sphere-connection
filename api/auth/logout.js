'use strict';

module.exports = async (req, res, next) => {
  res.status(200).clearCookie('s_auth').json({
    response: {
      status: 'success',
      message: 'Выход успешен'
    }
  });
}
