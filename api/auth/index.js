'use strict';

const closeUnauth = require('./closeUnauth');
const isAuth = require('./isAuth');
const login = require('./login');
const logout = require('./logout');
const registration = require('./registration');

module.exports = {
  closeUnauth,
  isAuth,
  login,
  logout,
  registration,
};
