'use strict';

const getInfo = require('./getInfo');
const updateMainInfo = require('./updateMainInfo');

module.exports = {
  getInfo,
  updateMainInfo
};
