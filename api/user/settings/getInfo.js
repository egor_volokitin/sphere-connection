'use strict';

const schemas = require('../../../bll/schemas/sequelize');

module.exports = async(req, res, next) => {
  const user = await schemas.Users.findOne({
    attributes: ['email', 'nickname', 'status', 'image'],
    where: {
      id: req.user.id
    }
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: user
    }
  });
}
