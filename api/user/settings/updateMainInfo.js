'use strict';

const schemas = require('../../../bll/schemas/sequelize');
const moment = require('moment');

module.exports = async (req, res, next) => {
  try {
    if(req.file) {
      await schemas.Users.update({
        image: req.file.location,
        status: req.body.status
      }, {
        where: {
          id: req.user.id
        }
      });
    }
    else {
      await schemas.Users.update({
        image: null,
        status: req.body.status
      }, {
        where: {
          id: req.user.id
        }
      });
    }

    res.status(200).json({
      response: {
        status: 'success',
        message: 'Данные успешно обновлены.'
      }
    });
  }
  catch(ex) {
    console.error(ex);

    res.status(200).json({
      response: {
        status: 'error',
        code: 'update-main-userinfo',
        message: 'Произошла ошибка при обновлении данных.'
      },
      request: {
        body: req.body,
        file: req.file,
        date: moment().utc().format('lll')
      }
    });
  }
};
