'use strict';

const schemas = require('../../../bll/schemas/sequelize');
const moment = require('moment');

module.exports = async (req, res, next) => {
  try {
    const refUser = await schemas.Users.findOne({
      attributes: ['id'],
      where: {
        uuid: req.body.refUuid
      },
      raw: true
    });
  
    if(!refUser) {
      res.status(200).json({
        response: {
          status: 'error',
          code: 'invalid-data',
          message: 'Не удается найти пользователя. Попробуйте позднее.'
        },
        request: {
          body: req.body,
          date: moment().utc().format('lll')
        }
      });
  
      return null;
    }
  
    let subscription = await schemas.Subscriptions.findOne({
      attributes: ['id'],
      where: {
        userId: req.user.id,
        refUserId: refUser.id
      },
      raw: true
    });
  
    await schemas.Subscriptions.destroy({
      where: {
        id: subscription.id
      }
    });

    res.status(200).json({
      response: {
        status: 'success',
        message: 'Вы успешно отписались от этого пользователя.'
      }
    });
  }
  catch(ex) {
    console.error(ex);

    res.status(200).json({
      response: {
        status: 'error',
        code: 'internal-error',
        message: 'Внутренняя ошибка сервера. Повторите запрос позднее.'
      },
      request: {
        body: req.body,
        date: moment().utc().format('lll')
      }
    });
  }

};
