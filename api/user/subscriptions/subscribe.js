'use strict';

const schemas = require('../../../bll/schemas/sequelize');
const moment = require('moment');

module.exports = async (req, res, next) => {
  try {
    const refUser = await schemas.Users.findOne({
      attributes: ['id'],
      where: {
        uuid: req.body.refUuid
      },
      raw: true
    });
  
    if(!refUser) {
      res.status(200).json({
        response: {
          status: 'error',
          code: 'invalid-data',
          message: 'Не удалось подписаться. Попробуйте позднее.'
        },
        request: {
          body: req.body,
          date: moment().utc().format('lll')
        }
      });
  
      return null;
    }
  
    await schemas.Subscriptions.findOrCreate({
      attributes: ['id'],
      where: {
        userId: req.user.id,
        refUserId: refUser.id
      },
      raw: true
    });
  
    res.status(200).json({
      response: {
        status: 'success',
        message: 'Вы успешно подписались на нового пользователя.'
      }
    });
  }
  catch(ex) {
    console.error(ex);

    res.status(200).json({
      response: {
        status: 'error',
        code: 'internal-error',
        message: 'Внутренняя ошибка сервера. Повторите запрос позднее.'
      },
      request: {
        body: req.body,
        date: moment().utc().format('lll')
      }
    });
  }

};
