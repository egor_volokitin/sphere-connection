'use strict';

const articles = require('./articles');
const getInfoForMyPage = require('./getInfoForMyPage');
const getUsersList = require('./getUsersList');
const settings = require('./settings');
const subscriptions = require('./subscriptions');

module.exports = {
  articles,
  getInfoForMyPage,
  getUsersList,
  settings,
  subscriptions
};
