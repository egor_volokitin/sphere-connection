'use strict';

const schemas = require('../../bll/schemas/sequelize');
const moment = require('moment');

module.exports = async (req, res, next) => {
  try {
    const user = await schemas.Users.findOne({
      attributes: ['nickname', 'image', 'fullRaiting', 'freeRaiting', 'status'],
      where: {
        id: req.user.id
      },
      raw: true
    });
  
    if(!user) {
      res.status(200).json({
        response: {
          status: 'error',
          code: 'invalid-link',
          message: 'Пользователь не найден'
        },
        request: {
          body: req.body,
          date: moment.utc().format('lll')
        }
      });
  
      return null;
    }

    const userArticles = await schemas.Articles.findAll({
      attributes: ['uuid', 'title', 'previewImage', ['shortDesc', 'shortDescription']],
      where: {
        published: req.body.published,
        userId: req.user.id
      },
      limit: 50,
      offset: (req.body.page - 1) * 50,
      order: [['createdAt', 'DESC']],
      raw: true
    });

    const bestArticles = await schemas.Articles.findAll({
      attributes: ['uuid', 'title', 'previewImage', 'likes'],
      where: {
        userId: req.user.id
      },
      limit: 5,
      order: [['likes', 'DESC']],
      raw: true
    });
  
    res.status(200).json({
      response: {
        status: 'success',
        message: {
          user: user,
          allArticles: userArticles,
          bestArticles: bestArticles
        }
      }
    });
  }
  catch (ex) {
    console.error(ex);

    res.status(200).json({
      response: {
        status: 'error',
        code: 'internal-error',
        message: 'Внутренняя ошибка сервера. Повторите позднее'
      },
      request: {
        body: req.body,
        date: moment.utc().format('lll')
      }
    });
  }
};
