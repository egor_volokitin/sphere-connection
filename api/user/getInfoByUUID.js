'use strict';

const schemas = require('../../bll/schemas/sequelize');
const moment = require('moment');

module.exports = async (req, res, next) => {
  try {
    const user = await schemas.Users.findOne({
      attributes: ['nickname', 'image', 'fullRaiting', 'freeRaiting', 'status'],
      where: {
        uuid: req.body.uuid
      },
      raw
    });
  
    if(!user) {
      res.status(200).json({
        response: {
          status: 'error',
          code: 'invalid-link',
          message: 'Пользователь не найден'
        },
        request: {
          body: req.body,
          date: moment.utc().format('lll')
        }
      });
  
      return null;
    }
  
    res.status(200).json({
      response: {
        status: 'success',
        message: user
      }
    });
  }
  catch (ex) {
    res.status(200).json({
      response: {
        status: 'error',
        code: 'internal-error',
        message: 'Внутренняя ошибка сервера. Повторите позднее'
      },
      request: {
        body: req.body,
        date: moment.utc().format('lll')
      }
    });
  }
};
