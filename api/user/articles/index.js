'use strict';

const createArticle = require('./create');
const getAll = require('./getAll');
const getTags = require('./getTags');

module.exports = {
  createArticle,
  getAll,
  getTags
};
