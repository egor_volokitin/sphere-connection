'use strict';

const Op = require('sequelize').Op;
const schemas = require('../../../bll/schemas/sequelize');

module.exports = async (req, res, next) => {
  let categories = await schemas.Categories.findAll({
    attributes: ['id', 'title'],
    where: {
      title: {
        [Op.iLike]: `%${req.body.searchLine}%`
      }
    },
    limit: 5,
    raw: true
  });

  res.status(200).json({
    response: {
      status: 'success',
      message:  categories
    }
  });
}
