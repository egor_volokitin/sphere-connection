'use strict';

const schemas = require('../../../bll/schemas/sequelize');
const generate = require('nanoid/generate');
const consts = require('../../../bll/schemas/constains');

module.exports = async (req, res) => {

  // Создаем новую статью
  let newArticle = await schemas.Articles.create({
    userId: req.user.id,
    title: req.body.title,
    content: req.body.content,
    shortDesc: req.body.shortDescription,
    previewImage: req.body.previewImage,
    published: req.body.published,
    commentsEnabled: req.body.commentsEnabled,
    uuid: generate(consts.nanoid.alphabet, 12)
  });
  
  res.status(200).json({
    response: {
      status: 'success',
      message: {
        article: {
          id: newArticle.dataValues.uuid,
          title: newArticle.dataValues.title
        }
      }
    }
  });
}
