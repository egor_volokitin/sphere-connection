'use strict';

const schemas = require('../../../bll/schemas/sequelize');

module.exports = async (req, res) => {
  let articles = await schemas.Articles.findAll({
    attributes: ['uuid', 'title', 'updatedAt'],
    where: {
      userId: req.user.id
    },
    order: ['createdAt'],
    raw: true
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: articles
    }
  });
};
