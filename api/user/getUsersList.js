'use strict';

const Op = require('sequelize').Op;
const schemas = require('../../bll/schemas/sequelize');

module.exports = async (req, res, next) => {
  const users = await schemas.Users.findAndCountAll({
    attributes: ['nickname', 'uuid', 'fullRaiting', 'freeRaiting', 'image'],
    where: {
      nickname: {
        [Op.iLike]: `%${req.body.searchLine}%`
      }
    },
    limit: 30,
    offset: parseInt(req.body.page - 1) * 50,
    raw: true
  });

  res.status(200).json({
    response: {
      status: 'success',
      message: users
    }
  });
};
